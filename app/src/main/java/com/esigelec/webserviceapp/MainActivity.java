package com.esigelec.webserviceapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;



public class MainActivity extends AppCompatActivity {
    TextView textMenue;
    Button pilotage;
    Button parametre;
    Button about;
    TextView webResponse;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textMenue = findViewById(R.id.textMenue);
        pilotage = findViewById(R.id.pilotage);
        parametre = findViewById(R.id.parametre);
        about = findViewById(R.id.about);
        webResponse = findViewById(R.id.webResponse);

        textMenue.setText("Choisissez le menue\npilotage pour piloter le robot\n" +
                "parametre pour regler la luminositer ou avoir des infromation sur les capteur\n" +
                "about pour avoir des information sur les developpeur");

        pilotage.setOnClickListener(view -> {
            Intent pilot = new Intent
                    (MainActivity.this,PilotageActivity.class);

            startActivity(pilot);
        });
        parametre.setOnClickListener(view -> {
            Intent param = new Intent
                    (MainActivity.this,ParametreActivity.class);

            startActivity(param);
        });
        about.setOnClickListener(view -> {
            Intent aboutAct = new Intent
                    (MainActivity.this,AboutActivity.class);

            startActivity(aboutAct);
        });

        String ip = null;
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (wifiManager != null) {
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            int ipAddress = wifiInfo.getIpAddress();
            String localStringIp = String.format("%d.%d.%d.%d", (ipAddress & 0xff), (ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff), (ipAddress >> 24 & 0xff));
            Log.d("IP Address", localStringIp);
            ip = localStringIp;
        }
        String url = "http://ip-api.com/json/";

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder(  )
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                final String webContent = response.body().string();
                webResponse = findViewById(R.id.webResponse);
                if (!response.isSuccessful()) {
                    Log.e("onResponse","erreur : " + response);

                    MainActivity.this.runOnUiThread(() -> ((TextView) findViewById(R.id.webResponse)).setText(""+response));
                    throw new IOException("Erreur: " + response);
                } else {
                    Log.d("onResponse", webContent);
                    MainActivity.this.runOnUiThread(() -> ((TextView) findViewById(R.id.webResponse)).setText(webContent));

                }
            }
        });
    }
}