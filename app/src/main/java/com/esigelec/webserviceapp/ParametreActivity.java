package com.esigelec.webserviceapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.provider.Settings;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import java.util.List;
import android.widget.SeekBar;

public class ParametreActivity extends AppCompatActivity  {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parametre);
        TextView textView = findViewById(R.id.textMenue);
        Switch auto = (Switch) findViewById(R.id.auto);
        SeekBar brightnessSeekBar = findViewById(R.id.brightness_seekbar);


        // Créer une instance de SensorManager
        SensorManager mySensorManager = (SensorManager)
                getSystemService(Context.SENSOR_SERVICE);

        // Récupérer la liste des capteurs
        List<Sensor> listeCapteurs = mySensorManager
                .getSensorList(Sensor.TYPE_ALL);

        TextView tv = (TextView) findViewById(R.id.textMenue);
        for (Sensor sensor : listeCapteurs) {
            tv.append(" - " + sensor.getType()
                    + "\t: \t" + sensor.getName() + "\n");
        }

        // Obtenir le capteur gyroscope par défaut,
        // retourne NULL s'il n'existe pas
        Sensor light = mySensorManager
                .getDefaultSensor(Sensor.TYPE_LIGHT);
        if (light == null) {
            Toast.makeText(this, "Pas de capteur de luminosité !", Toast.LENGTH_LONG)
                    .show();
        } else {
            Toast.makeText(this, "Il existe au moins capteur de luminosité.",
                    Toast.LENGTH_LONG).show();
        }

        brightnessSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // Mettre à jour la luminosité de l'écran en utilisant la valeur actuelle de la SeekBar
                int brightnessValue = progress ;//* 255 / 100; // Convertir la valeur de 0-100 en une valeur de 0-255
                WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
                layoutParams.screenBrightness = brightnessValue / 255f;
                getWindow().setAttributes(layoutParams);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        Sensor lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        if(lightSensor != null){
            auto.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isCheck) {
                    if(isCheck)
                    {
                        Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC);
                    }
                    else
                    {
                        Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);

                    }
                }
            });


        }
        else
        {
            auto.setClickable(false);
            auto.setBackgroundColor(0x777777);
        }
    }



}


