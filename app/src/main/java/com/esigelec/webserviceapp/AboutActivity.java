package com.esigelec.webserviceapp;

import androidx.appcompat.app.AppCompatActivity;

import android.media.Image;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class AboutActivity extends AppCompatActivity {
 TextView noms;
 ImageView robot;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        noms = findViewById(R.id.noms);
        noms.setText("Heidi MEZIANE\nFabrice KAYO\nJason SIOCHE\nPatrick KOPANSKY");

        robot = findViewById(R.id.robot);

    }
}