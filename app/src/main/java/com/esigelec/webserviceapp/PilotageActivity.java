package com.esigelec.webserviceapp;

        import androidx.appcompat.app.AppCompatActivity;

        import android.Manifest;
        import android.bluetooth.BluetoothAdapter;
        import android.bluetooth.BluetoothDevice;
        import android.content.BroadcastReceiver;
        import android.content.Context;
        import android.content.Intent;
        import android.content.IntentFilter;
        import android.hardware.Sensor;
        import android.hardware.SensorEvent;
        import android.hardware.SensorEventListener;
        import android.os.Build;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.AdapterView;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.ListView;
        import android.widget.TextView;
        import android.util.Log;
        import java.io.IOException;
        import java.nio.charset.Charset;
        import java.util.ArrayList;
        import java.util.Calendar;
        import java.util.Set;
        import java.util.UUID;

        import okhttp3.OkHttpClient;
        import okhttp3.Request;
        import java.sql.Timestamp;





/*
public class MainActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btn = (Button) findViewById(R.id.lumax);
        btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Appel du manager de connexion
                ConnectivityManager connectivityManager =
                        (ConnectivityManager) getSystemService (CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                TextView tv =  (TextView) findViewById(R.id. textView1);
                // Retourne le type de connexion mobile ou WiFi
                if (networkInfo != null)
                {
                    tv.setText(networkInfo.getTypeName());
                }
                else
                {
                    tv.setText("Pas de réseau !");
                }
            }
        });
    }
}*/

public class PilotageActivity extends AppCompatActivity implements SensorEventListener {

    private static final String TAG = "PilotageActivity";

    BluetoothAdapter mBluetoothAdapter;
    BluetoothConnectionService mBluetoothConnectionService;

    Button btnONOFF;
    Button btnDiscovery;
    Button btnDiscover;
    Button btnStartConnection;
    Button left_button;
    Button back_button;
    Button right_button;
    Button forward_button;
    float l;


    private static final UUID MY_UUID_INSECURE =
            UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");


    BluetoothDevice mBluetoothDevice;

    public ArrayList<BluetoothDevice> mBTDevices = new ArrayList<>();
    public DeviceListAdapter mdeviceListAdapter;
    ListView lvNewDevices;

    //je ne connais toujours pas à quoi ca sert
    private final BroadcastReceiver mBroadcastReceiver1 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(mBluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, mBluetoothAdapter.ERROR);

                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        Log.d(TAG, "onreceive: STATE OFF");
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        Log.d(TAG, "mBroadcastReceiver1: STATE TURNING OFF");
                        break;
                    case BluetoothAdapter.STATE_ON:
                        Log.d(TAG, "mBroadcastReceiver1: STATE ON");
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        Log.d(TAG, "mBroadcastReceiver1: STATE TURNING ON");
                        break;
                }
            }
        }
    };

    /**
     * Broadcast Receiver for change mode to bluetooth states such as:
     * 1) Discoverability lode on/off of expire.
     */
    private final BroadcastReceiver mBroadcastReceiver2 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(mBluetoothAdapter.ACTION_SCAN_MODE_CHANGED)) {
                final int mode = intent.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE, mBluetoothAdapter.ERROR);

                switch (mode) {
                    case BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE:
                        Log.d(TAG, "mBroadcastReceiver2: Discoverability Enabled.");
                        break;
                    case BluetoothAdapter.SCAN_MODE_CONNECTABLE:
                        Log.d(TAG, "mBroadcastReceiver2: Discoverability Enabled. Able to receive connections.");
                        break;
                    case BluetoothAdapter.SCAN_MODE_NONE:
                        Log.d(TAG, "mBroadcastReceiver2: Discoverability disabled. Not able to receive connections.");
                        break;
                    case BluetoothAdapter.STATE_CONNECTING:
                        Log.d(TAG, "mBroadcastReceiver2: connecting");
                        break;
                    case BluetoothAdapter.STATE_CONNECTED:
                        Log.d(TAG, "mBroadcastReceiver2: connected");
                        break;
                }
            }
        }
    };

    /**
     * listening devices that are not yet paired
     * executed by btnDiscover() method.
     */
    private BroadcastReceiver mBroadcastReceiver3 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            checkBTPermission();
            Log.d(TAG, "onReceive: ACTION FOUND.");


            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                mBTDevices.add(device);
                Log.d(TAG, "onReceive: " + device.getName() + ":" + device.getAddress());
                mdeviceListAdapter = new DeviceListAdapter(context, R.layout.device_adapter_view, mBTDevices);
                lvNewDevices.setAdapter(mdeviceListAdapter);

            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                // La découverte des appareils Bluetooth est terminée
                Log.d(TAG, "onReceive: ACTION END.");
                //BTDevices.clear();
            } else {
                Log.d(TAG, "onReceive: NONE.");
            }

        }
    };

    /**
     * Broadcast Receiver that detects bond state changes (Pairing status changes)
     */
    private final BroadcastReceiver mBroadcastReceiver4 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "BroadcastReceiver: action1.");
            final String action = intent.getAction();
            checkBTPermission();
            Log.d(TAG, "BroadcastReceiver: action2.");
            if (action.equals(BluetoothDevice.ACTION_BOND_STATE_CHANGED)) {
                Log.d(TAG, "BroadcastReceiver: action3.");
                BluetoothDevice mDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                //3 cases:
                //case1: bonded already
                if (mDevice.getBondState() == BluetoothDevice.BOND_BONDED) {
                    Log.d(TAG, "BroadcastReceiver: BOND_BONDED.");
                    mBluetoothDevice = mDevice;
                }
                //case2: creating a bone
                if (mDevice.getBondState() == BluetoothDevice.BOND_BONDING) {
                    Log.d(TAG, "BroadcastReceiver: BOND_BONDING.");
                }
                //case3: breaking a bond
                if (mDevice.getBondState() == BluetoothDevice.BOND_NONE) {
                    Log.d(TAG, "BroadcastReceiver: BOND_NONE.");
                }
            }
        }
    };

    @Override
    protected void onDestroy() {
        Log.d(TAG, "o,Destroy: called");
        super.onDestroy();
        checkBTPermission();
        mBluetoothAdapter.cancelDiscovery();
       /* unregisterReceiver(mBroadcastReceiver1);
        unregisterReceiver(mBroadcastReceiver2);
        unregisterReceiver(mBroadcastReceiver3);
        unregisterReceiver(mBroadcastReceiver4);*/
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilotage);


        mBluetoothConnectionService = new BluetoothConnectionService(PilotageActivity.this);

        btnONOFF = (Button) findViewById(R.id.button_activate);
        btnDiscovery = (Button) findViewById(R.id.button_discov);
        btnDiscover = findViewById(R.id.button_discover);
        btnStartConnection = findViewById(R.id.button_startConnection);
        forward_button = findViewById(R.id.forward_button);
        left_button = findViewById(R.id.left_button);
        right_button = findViewById(R.id.right_button);
        back_button = findViewById(R.id.back_button);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        lvNewDevices = (ListView) findViewById(R.id.lvNewDevices);
        mBTDevices = new ArrayList<>();


        AdapterView.OnItemClickListener PilotageActivity = null;
        lvNewDevices.setOnItemClickListener(PilotageActivity);

        //Broadcasts when bond state changes (ie:pairing)
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        registerReceiver(mBroadcastReceiver4, filter);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();


        btnONOFF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: enabling/disabling blutooth. ");
                enableDisableBT();
            }
        });

        btnDiscovery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: enabling/disabling discovery.");
                btnEnableDisable_Discoverable(v);
            }
        });

        btnDiscover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkBTPermission();
                mBTDevices.clear();
                Log.d(TAG, "onClick: discover");
                btnDiscover(v);
                Log.d(TAG, "onClick: discover2");
                //lvNewDevices.setOnItemClickListener(MainActivity.this);

                Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
                Log.d(TAG, "onClick: discover3" + pairedDevices.size());
                // Si des appareils sont appariés, affichez-les dans la console
                if (pairedDevices.size() > 0) {
                    for (BluetoothDevice device : pairedDevices) {
                        Log.d(TAG, "Device Name: " + device.getName() + ", Device Address: " + device.getAddress());
                    }
                }
            }
        });

        btnStartConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startConnection();

            }
        });

        OkHttpClient client = new OkHttpClient();
        // creating a Calendar object
        Calendar date = Calendar.getInstance();

        forward_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://cabani.free.fr/ise/adddata.php?idproject=11&lux="+l+"&timestamp="+ date.getTime() +"&action=forward";
                String dir = String.valueOf('z');
                byte[] bytes = dir.getBytes(Charset.defaultCharset());
                mBluetoothConnectionService.write(bytes);
                Request request = new Request.Builder()
                        .url(url)
                        .build();
            }
        });

        left_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://cabani.free.fr/ise/adddata.php?idproject=11&lux="+l+"&timestamp="+ date.getTime() +"&action=left";
                String dir = String.valueOf('q');
                byte[] bytes = dir.getBytes(Charset.defaultCharset());
                mBluetoothConnectionService.write(bytes);
                Request request = new Request.Builder()
                        .url(url)
                        .build();
            }
        });

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://cabani.free.fr/ise/adddata.php?idproject=11&lux="+l+"&timestamp="+ date.getTime() +"&action=back";
                String dir = String.valueOf('s');
                byte[] bytes = dir.getBytes(Charset.defaultCharset());
                mBluetoothConnectionService.write(bytes);
                Request request = new Request.Builder()
                        .url(url)
                        .build();
            }
        });

        right_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://cabani.free.fr/ise/adddata.php?idproject=11&lux="+l+"&timestamp="+ date.getTime() +"&action=right";
                String dir = String.valueOf('d');
                byte[] bytes = dir.getBytes(Charset.defaultCharset());
                mBluetoothConnectionService.write(bytes);
                Request request = new Request.Builder()
                        .url(url)
                        .build();
            }
        });


    }

    //create method for starting connection
//***remember the connecction will fail and app will crash if you haven't paired first
    public void startConnection() {
        startBTConnection(mBluetoothDevice, UUID.fromString(mBluetoothDevice.getUuids()[0].toString()));
    }


    /**
     * starting chat service method
     */
    public void startBTConnection(BluetoothDevice device, UUID uuid) {
        Log.d(TAG, "startBTConnection: Initializing RFCOM Bluetooth Connection.");

        mBluetoothConnectionService.startClient(device, uuid);
    }


    private void enableDisableBT() {
        checkBTPermission();
        if (mBluetoothAdapter == null) {
            Log.d(TAG, "enableDisableBT: Does not have BT capabilities.");
        }
        if (!mBluetoothAdapter.isEnabled()) {
            Log.d(TAG, "enableDisableBT: enabling BT");
            Intent enableBTIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);

            startActivity(enableBTIntent);

            IntentFilter BTIntent = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
            registerReceiver(mBroadcastReceiver1, BTIntent);
        }
        if (mBluetoothAdapter.isEnabled()) {
            Log.d(TAG, "enableDisableBT: disabling BT");
            mBluetoothAdapter.disable();

            IntentFilter BTIntent = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
            registerReceiver(mBroadcastReceiver1, BTIntent);

        }
    }

    public void btnEnableDisable_Discoverable(View view) {
        Log.d(TAG, "btnEnableDisable_Discoverable: making device discoverable for 300 seconds.");

        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
        checkBTPermission();
        startActivity(discoverableIntent);


        IntentFilter intentFilter = new IntentFilter(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED);
        registerReceiver(mBroadcastReceiver2, intentFilter);
    }

    public void btnDiscover(View view) {

        if (mBluetoothAdapter.isEnabled()) {
            // L'adaptateur Bluetooth est activé, vous pouvez continuer
        } else {
            // L'adaptateur Bluetooth n'est pas activé, vous devez l'activer avant de continuer
            enableDisableBT();
        }
        Log.d(TAG, "btnDiscover: loking for unpaired devices.");
        checkBTPermission();


        if (mBluetoothAdapter.isDiscovering()) {
            mBluetoothAdapter.cancelDiscovery();
            Log.d(TAG, "btnDiscover: canceling discovery.");

            checkBTPermission();
           /* mBluetoothAdapter.startDiscovery();
            IntentFilter discoverDevicesIntent = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            registerReceiver(mBroadcastReceiver3, discoverDevicesIntent);

            */


        } else {
            Log.d(TAG, "btnDiscover: start discovery.");
            //checkBTPermission();

            mBluetoothAdapter.startDiscovery();
            IntentFilter discoverDevicesIntent = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            registerReceiver(mBroadcastReceiver3, discoverDevicesIntent);
            //registerReceiver(mBroadcastReceiver3, new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED));
        }
    }

    /**
     * This methode is required for all devices running API23+
     * Android must programmatically check the permission for bluetooth. Putting the proper permissions
     * in the manifest is not enough.
     */
    private void checkBTPermission() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            int permissionCheck = this.checkSelfPermission("Manifest.permission.ACCESS_FINE_LOCATION");
            permissionCheck += this.checkSelfPermission("Manifest.permission.ACCESS_COARSE_LOCATION");
            permissionCheck += this.checkSelfPermission("Manifest.permission.BLUETOOTH_SCAN");

            if (permissionCheck != 0) {
                this.requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.BLUETOOTH_SCAN}, 1001);
            }
        } else {
            Log.d(TAG, "checkBTPermission: No need to check permissions. SDK version < LOLLIPOP.");
        }
    }


    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        checkBTPermission();
        //first cancel discovery because its very memory intensive.
        mBluetoothAdapter.cancelDiscovery();

        Log.d(TAG, "onItemClick: You Clicked on a device.");
        String deviceName = mBTDevices.get(i).getName();
        String deviceAddress = mBTDevices.get(i).getAddress();

        Log.d(TAG, "onItemClick: deviceName = " + deviceName);
        Log.d(TAG, "onItemClick: deviceAddress = " + deviceAddress);

        //create the bond.
        //NOTE: Requires API 17+? I think this is JellyBean
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2) {
            Log.d(TAG, "Trying to pair with " + deviceName);
            try {
                mBTDevices.get(i).createBond();
                // mBTDevices.get(i).getUuids();
                Log.d(TAG, "Trying to createBond  " + mBTDevices.get(i).createBond());
            } catch (Exception e) {
                Log.d(TAG, "Trying to createBond  " + mBTDevices.get(i).createBond());
            }

            Log.d(TAG, "Trying to connect to  " + mBTDevices.get(i).getName());
            mBluetoothDevice = mBTDevices.get(i);

            Log.d(TAG, "Trying to connect to  " + mBluetoothDevice.getUuids()[1].toString());
            Log.d(TAG, "Trying to connect to  " + mBluetoothDevice.getName());


        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        // Mettre à jour uniquement dans le cas de notre capteur
        if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
            // La valeur de la lumière
            l = event.values[0];
        }


    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
